:experimental:
:checkpoint-sha: 035bb6d5c47d9d0233b007c25ed598e26cba27b0

= Basics

**Covered features**: link:https://www.terraform.io/docs/cli/commands/init.html[initializing command], link:https://www.terraform.io/docs/cli/run/index.html[provisioning commands]

include:::partial$checkpoint.adoc[]

== Check requirements

=== `terraform`

✏ Let's verify your `terraform` binary is working

[.copy]
[source,bash]
----
Λ\: $ terraform version
----

(Example output)

[source,bash]
----
Terraform v1.0.6 # <1>
on linux_amd64

Your version of Terraform is out of date! The latest version
is 1.0.7. You can update by downloading from https://www.terraform.io/downloads.html
----
<1> Your version may differ

NOTE: If `terraform` complains about *not be being up to date* as shown above, you can safely *ignore it*.

=== `docker`

✏ Let's verify your `docker` setup: 

[.copy]
[source,bash]
----
Λ\: $ docker version
----

(Example output)

[source,bash]
----
Client:
 Version:           20.10.10
 API version:       1.41
 Go version:        go1.17.2
 Git commit:        b485636f4b
 Built:             Tue Oct 26 03:44:01 2021
 OS/Arch:           linux/amd64
 Context:           default
 Experimental:      true

Server:
 Engine:
  Version:          20.10.10 # <1>
  API version:      1.41 (minimum version 1.12)
  Go version:       go1.17.2
  Git commit:       e2f740de44
  Built:            Tue Oct 26 03:43:48 2021
  OS/Arch:          linux/amd64
  Experimental:     false
 containerd:
  Version:          v1.5.7
  GitCommit:        8686ededfc90076914c5238eb96c883ea093a8ba.m
 runc:
  Version:          1.0.2
  GitCommit:        v1.0.2-0-g52b36a2d
 docker-init:
  Version:          0.19.0
  GitCommit:        de40ad0
----
<1> Your version may differ


== Common commands

._There are five essentials command(ment)s you need to master!_
[quote, The Colonel]
image:colonel-five-fingers.png[]

=== `init`

The link:https://www.terraform.io/docs/cli/commands/init.html[`terraform init`,window="_blank"] command is used to initialize a working directory containing Terraform configuration files. This is the first command that should be run after writing a new Terraform configuration or cloning an existing one from version control. It is safe to run this command multiple times as it is idempotent.

NOTE: You will be *using the `init`* command in the *next xref:_prepare_local_environment[section]* to initialize everything

[[_plan_cmd]]
=== `plan`

The link:https://www.terraform.io/docs/cli/commands/plan.html[`terraform plan`,window="_blank"] command creates an execution plan. By default, creating a plan consists of:

* Reading the current state of any already-existing remote objects to make sure that the Terraform state is up-to-date.

* Comparing the current configuration to the prior state and noting any differences.

* Proposing a set of change actions that should, if applied, make the remote objects match the configuration.

=== `apply`

The link:https://www.terraform.io/docs/cli/commands/apply.html[`terraform apply`,window="_blank"] command executes the actions proposed in a Terraform plan.

The most straightforward way to use `terraform apply` is to run it without any arguments at all, in which case it will automatically create a new execution plan (as if you had run xref:_plan_cmd[terraform plan]) and then prompt you to approve that plan, before taking the indicated actions.

[TIP]
====
To *apply a previously saved plan* you created with the xref:_plan_cmd[terraform plan] command, simply add the path to the plan file as command line argument to your `terraform apply` call:

[source,bash]
----
Λ\: $ terraform apply path/to/terraform.plan
----

====

=== `destroy`

The link:https://www.terraform.io/docs/cli/commands/destroy.html[`terraform destroy`,window="_blank"] command is a convenient way to destroy all remote objects managed by a particular Terraform configuration.

While you will typically not want to destroy long-lived objects in a production environment, Terraform is sometimes used to manage ephemeral infrastructure for development purposes, in which case you can use terraform destroy to conveniently clean up all of those temporary objects once you are finished with your work.

CAUTION: Use the `destroy` command with *caution* as it does *destroy* resources. In this lab, you will use it multiple times 😈

=== `workspaces`

[[_workspaces-cli]]
This command is meant to create, delete and switch between multiple targeted environments when deploying infrastructure. It will be covered in depth in the xref:07_workspaces.adoc[dedicated section] later during this course.

[[_prepare_local_environment]]
== Prepare your local environment

✏ Create a file called `main.tf` and add the following content to it

[.copy]
[source,hcl]
----
# Content of main.tf
terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.15.0"
    }
  }
}
----

NOTE: The notion of *providers* will be *explained in the next chapters*. For now, simply follow the instructions to get you started.

[.copy]
[source,bash]
----
Λ\: $ terraform init
----
(Example output)

[source,bash]
----
Initializing the backend...

Initializing provider plugins...
- Finding kreuzwerker/docker versions matching "2.15.0"...
- Installing kreuzwerker/docker v2.15.0...
- Installed kreuzwerker/docker v2.15.0 (self-signed, key ID BD080C4571C6104C)

Partner and community providers are signed by their developers.
If you'd like to know more about provider signing, you can read about it here:
https://www.terraform.io/docs/cli/plugins/signing.html

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
----

👀 Check your local file tree

[TIP]
====
Either *use* your *file navigator* or use the *`tree`* command if you are running on Linux:
[.copy]
[source,bash]
----
Λ\: $ tree -a
----
====

image:init-terraform-folder-tree.png[]

👀 You should notice a `.terraform` folder and a `.terraform.lock.hcl` file have been created
