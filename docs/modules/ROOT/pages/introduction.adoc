:experimental:

= Introduction

Welcome to this `terraform` course (*TFC*) image:https://gitlab.com/uploads/-/system/project/avatar/45600820/terraform-training.png[Terraform Logo,width="30px"] !

In this course, you will learn how to:

* Define your infrastructure as code
* Use the `terraform` command line interface to plan, apply and tear down resources
* Leverage the use of providers
* Configure a backend to collaborate effectively
* Deploy a serverless application in the cloud
* Help the colonel build a fast food empire!


At the end of this lab, you will be able to define your infrastructures as code as well as understand and contribute to existing ones.

== Audience

This workshop is meant for *developers* as a first hands-on experience with `terraform`. The lab is based on specific `terraform` provider that makes it possible to interact with `docker` using `terraform`, in a a way that is similar to what `docker-compose` does. A minimal understanding of `docker` is better though no considerable hands-on experience is required.

NOTE: *No extensive knowledge of `docker`* is necessary. Don't worry, your trainer is here to assist you on every step of the way. 

== Setting up your environment

To follow this course, you need to:

* Install `docker` by following the link:https://docs.docker.com/get-docker/[official documentation,window="_blank"] depending on your platform
* Install `terraform` with the link:https://www.terraform.io/downloads.html[official instructions,window="_blank"]

To execute locally most of the commands required during this course, you need to have the following tooling installed:

* A minimal text editor such as vscode, pycharm, notepad++, `vim` etc ...

Congratulations, you're now ready to help the colonel get on track to open his first restaurants! 🐔

._This adventure's gonna be terraform licking good!_
[quote, The Colonel]
image:colonel-in-chair.jpg[]

_Made with ❤ by the link:https://gitlab.com/nikola.lohinski[Agent K,window="_blank"]_
