variable "google_project" {
  type        = string
}

variable "google_region" {
  type        = string
  default     = "europe-west6"
}