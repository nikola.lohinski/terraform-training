resource "random_string" "account_suffix" {
  length           = 10
  special          = false
  number           = false
  lower            = true
  upper            = false
}

resource "google_service_account" "course_account" {
  account_id   = "terraform-course-${random_string.account_suffix.result}"
  display_name = "[DO NOT MODIFY MANUALLY]: Terraform course service account managed by GitLab CI/CD"
}

resource "google_service_account_key" "course_key" {
  service_account_id = google_service_account.course_account.name
}

resource "local_file" "course_credentials" {
    content     = base64decode(google_service_account_key.course_key.private_key)
    filename = "${path.module}/google-application-credentials.json"
}