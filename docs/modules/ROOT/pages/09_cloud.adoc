:experimental:
:checkpoint-sha: 820de07c02b85090c69060a1dfee0003e65b200e

= Cloud

**Covered features**: link:https://registry.terraform.io/providers/hashicorp/google/latest/docs[google provider]

include:::partial$checkpoint.adoc[]

== Understanding the `google` provider

Now that we have covered all the basics, you should be able to deploy resources remotely using `terraform`. As you have seen in a xref:05_providers.adoc#_random_provider[previous section with the `random` provider] or throughout the course with the `docker` provider, `terraform` can interact with any number of external tools seamlessly.

In this chapter, you will be using a new provider able to interact with the Google Cloud Platform (or GCP): the link:https://registry.terraform.io/providers/hashicorp/google/latest/docs[`google` provider].

NOTE: *No need to worry about knowing GCP* and how to work with it. Everything you need to known is detailed and the end goal is to show you that *everything you did* with the `docker` provider *is applicable to any other provider*

We'll cover three resource from the `google` provider in this section:

- The link:https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_service[Cloud Run service resource] to deploy an HTTP accessible container in minutes within link:https://cloud.google.com/run[Cloud Run]

- link:https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_service_iam#google_cloud_run_service_iam_member[Cloud Run service IAM member resource] to allow any user to access your running container with link:https://cloud.google.com/iam[Identity and Access Management (referred as IAM)] rules

- The link:https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket[Storage Bucket resource] to create a bucket within GCP 

._I am feeling so crispy with success! It's time to shoot for the stars buddy, and make me reach the cloud!_
[quote, The Colonel]
image:colonel-astronaute.png[]

== Configuring access and targets

As mentioned in xref:05_providers.adoc#_configure_docker_provider[the previous chapters], some providers require a bit of configuration.

✏ Add the following lines to your `main.tf` file:

[.copy]
[source,hcl]
----
variable "project" {
  type        = string
  default     = "terraform-course"
}

variable "region" {
  type        = string
  default     = "europe-west6"
}

variable "name" {
    type = string
    default = "<your-name>" # <1>
}

provider "google" {
  project     = var.project
  region      = var.region
  credentials = "/path/to/your/credentials/file.json" # <2>
}
----
<1> Either fill in your own dash (`"-"`) separated name or define the `TF_VAR_name` environment variable in your current shell
<2> Use the credentials you used in the xref:08_backend.adoc#_get_credentials_from_trainer[previous section] by stating the path on your local machine to the JSON file

[TIP]
====
Should you need to define a *new environment variable*, simply use the `export` command: 

[.copy]
[source,bash]
----
Λ\: $ export TF_VAR_name="YOUR-NAME" # <1>
----
<1> Fill in your own dash (`"-"`) separated name
====

NOTE: You *can also define* the `GOOGLE_APPLICATION_CREDENTIALS` *environment variable* referring *to your credentials file* rather than hard coding the path in your `main.tf`

✏ Reinitialize `terraform`: 

[.copy]
[source,bash]
----
Λ\: $ terraform init
----

(Output)

[source,bash]
----
Initializing the backend...

Initializing provider plugins...
- Reusing previous version of kreuzwerker/docker from the dependency lock file
- Reusing previous version of hashicorp/random from the dependency lock file
- Finding latest version of hashicorp/google...
- Using previously-installed kreuzwerker/docker v2.15.0
- Using previously-installed hashicorp/random v3.1.0
- Installing hashicorp/google v4.1.0... 
- Installed hashicorp/google v4.1.0 (signed by HashiCorp) # <1>

Terraform has made some changes to the provider dependency selections recorded
in the .terraform.lock.hcl file. Review those changes and commit them to your
version control system if they represent changes you intended to make.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
----
<1> You can see that `terraform` detected that you now need the `google` provider and retrieved the plugin

== Deploying to the cloud

It is time to deploy resources in the cloud, as easily as we did with local docker containers!

✏ First of all, remove the `minio.tf` file as we will not be deploying the `minio_tf` container locally anymore

[.copy]
[source,bash]
----
Λ\: $ rm minio.tf
----

✏ Apply the configuration: 

[.copy]
[source,bash]
----
Λ\: $ terraform apply -auto-approve
----

(Output)

[source,bash]
----
random_pet.minio_user: Refreshing state... [id=steadily-proven-octopus]
random_integer.minio_port: Refreshing state... [id=9175]
random_password.minio_password: Refreshing state... [id=none]
docker_container.minio_tf: Refreshing state... [id=ecc0ddfef09ffd3f619a5df57e623ece91aaeb3d56551919951e3886d81dddb0]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following
symbols:
  - destroy

Terraform will perform the following actions:

  # docker_container.minio_tf will be destroyed
  - resource "docker_container" "minio_tf" {
      - attach            = false -> null
      - bridge            = "" -> null
      - command           = [
          - "server",
          - "/data",
          - "--console-address",
          - ":9001",
        ] -> null
      - cpu_set           = "" -> null
      - cpu_shares        = 0 -> null
      - dns               = [] -> null
      - dns_opts          = [] -> null
      - dns_search        = [] -> null
      - domainname        = "" -> null
      - entrypoint        = [
          - "/usr/bin/docker-entrypoint.sh",
        ] -> null
      - env               = (sensitive) -> null
      - gateway           = "172.17.0.1" -> null
      - group_add         = [] -> null
      - hostname          = "ecc0ddfef09f" -> null
      - id                = "ecc0ddfef09ffd3f619a5df57e623ece91aaeb3d56551919951e3886d81dddb0" -> null
      - image             = "sha256:f25e9f95f0ba4449fc8dcfc19e7bbe486a889688ce7654480116056e2fc1751a" -> null
      - init              = false -> null
      - ip_address        = "172.17.0.3" -> null
      - ip_prefix_length  = 16 -> null
      - ipc_mode          = "private" -> null
      - links             = [] -> null
      - log_driver        = "json-file" -> null
      - log_opts          = {} -> null
      - logs              = false -> null
      - max_retry_count   = 0 -> null
      - memory            = 0 -> null
      - memory_swap       = 0 -> null
      - must_run          = true -> null
      - name              = "minio-container-expand" -> null
      - network_data      = [
          - {
              - gateway                   = "172.17.0.1"
              - global_ipv6_address       = ""
              - global_ipv6_prefix_length = 0
              - ip_address                = "172.17.0.3"
              - ip_prefix_length          = 16
              - ipv6_gateway              = ""
              - network_name              = "bridge"
            },
        ] -> null
      - network_mode      = "default" -> null
      - pid_mode          = "" -> null
      - privileged        = false -> null
      - publish_all_ports = false -> null
      - read_only         = false -> null
      - remove_volumes    = true -> null
      - restart           = "no" -> null
      - rm                = false -> null
      - security_opts     = [] -> null
      - shm_size          = 64 -> null
      - start             = true -> null
      - stdin_open        = false -> null
      - storage_opts      = {} -> null
      - sysctls           = {} -> null
      - tmpfs             = {} -> null
      - tty               = false -> null
      - user              = "" -> null
      - userns_mode       = "" -> null
      - working_dir       = "" -> null

      - ports {
          - external = 9175 -> null
          - internal = 9001 -> null
          - ip       = "0.0.0.0" -> null
          - protocol = "tcp" -> null
        }

      - volumes {
          - container_path = "/data" -> null
          - host_path      = "/home/user/Documents/terraform-course/supplies" -> null
          - read_only      = false -> null
        }
    }

  # random_integer.minio_port will be destroyed
  - resource "random_integer" "minio_port" {
      - id     = "9175" -> null
      - max    = 9999 -> null
      - min    = 9000 -> null
      - result = 9175 -> null
      - seed   = "expand" -> null
    }

Plan: 0 to add, 0 to change, 2 to destroy. # <1>
docker_container.minio_tf: Destroying... [id=ecc0ddfef09ffd3f619a5df57e623ece91aaeb3d56551919951e3886d81dddb0]
docker_container.minio_tf: Destruction complete after 1s
random_integer.minio_port: Destroying... [id=9175]
random_integer.minio_port: Destruction complete after 0s

Apply complete! Resources: 0 added, 0 changed, 2 destroyed.

Outputs:

root_password = <sensitive>
root_user = "steadily-proven-octopus"
----
<1> We removed the random port and the running container so terraform takes care of cleaning the state

WARNING: The last command you run *took care* of your *current workspace only*. If you wish to clean the other workspaces too, you need to switch between them. *This is optional* as this section will *not make you switch* between workspaces so you may keep doing everything in the current one.

=== Creating a Cloud Storage bucket

:checkpoint-sha: c2c8d22d6043bff9d97c5b21778c90d254681f72

include:::partial$checkpoint.adoc[]

✏ Create a file called `bucket.tf` and paste the following content inside: 

[.copy]
[source,hcl]
----
resource "google_storage_bucket" "bucket_tf" {
  name          = "terraform-course-${var.name}-${terraform.workspace}" # <1>
  location      = upper(var.region) # <2>
  force_destroy = true # <3>
}
----
<1> The name of the bucket, made _workspace-friendly_ and using your name to avoid conflicting with other attendees
<2> The location of the bucket, using the same region as the provider. The requirements of this resource specify the value needs to be uppercase, which is done using the link:https://www.terraform.io/docs/language/functions/upper.html[`upper` built-in function]
<3> This parameter allows the bucket to be destroyed even if there are still files inside

✏ Apply the configuration

[.copy]
[source,bash]
----
Λ\: $ terraform apply -auto-approve
----

(Output)

[source,bash]
----
Λ\: $ 
random_pet.minio_user: Refreshing state... [id=steadily-proven-octopus]
random_password.minio_password: Refreshing state... [id=none]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following
symbols:
  + create

Terraform will perform the following actions:

  # google_storage_bucket.bucket_tf will be created
  + resource "google_storage_bucket" "bucket_tf" {
      + force_destroy               = true
      + id                          = (known after apply)
      + location                    = "EUROPE-WEST6"
      + name                        = "terraform-course-agent-k-expand" # <1>
      + project                     = (known after apply)
      + self_link                   = (known after apply)
      + storage_class               = "STANDARD"
      + uniform_bucket_level_access = (known after apply)
      + url                         = (known after apply)
    }

Plan: 1 to add, 0 to change, 0 to destroy.
google_storage_bucket.bucket_tf: Creating...
google_storage_bucket.bucket_tf: Creation complete after 1s [id=terraform-course-agent-k-expand]

Apply complete! Resources: 1 added, 0 changed, 0 destroyed.

Outputs:

root_password = <sensitive>
root_user = "steadily-proven-octopus"
----
<1> The resolved name of your bucket

🎊 That's it! You created your first remote resource using `terraform`.

[TIP]
====
If you *want to check* you have *deployed the bucket* correctly, either *ask your trainer* to show you the console, or *use `gsutil`* as follows:

[.copy]
[source,bash]
----
Λ\: $ gsutil ls -L -b gs://NAME-OF-THE-BUCKET/ # <1>
----
<1> Use the name you defined in the `bucket.tf` file which should look like `terraform-course-YOUR-NAME-WORKSPACE`
====

=== Running `minio` in Cloud Run

:checkpoint-sha: 298193740ade3c3352d5ad8fa7c1d2a66357a148

include:::partial$checkpoint.adoc[]

You are about to deploy a `minio` container in Google's Cloud Run. It will be connected to the same projects Cloud Storage service to allow you to access buckets from the `minio` Web UI.

NOTE: Pretty cool being able to *serve buckets remotely* from a custom server, right ? 😎

✏ Create a file named `cloud-run.tf` and paste the following content inside: 

[.copy]
[source,hcl]
----
resource "google_cloud_run_service" "cloud_minio" { # <1>
  name     = "minio-${var.name}-${terraform.workspace}" # <2>
  location = var.region

  template {
    spec {
      containers {
        image = "registry.hub.docker.com/minio/minio:latest" # <3>

        env {
          name  = "MINIO_ROOT_USER"
          value = random_pet.minio_user.id
        }

        env {
          name  = "MINIO_ROOT_PASSWORD"
          value = random_password.minio_password.result
        }

        command = ["/usr/bin/docker-entrypoint.sh"]

        ports {
          container_port = 9001
        }

        args = [
          "minio", "gateway", "gcs", var.project, "--console-address", "0.0.0.0:9001" # <4>
        ]
      }
    }
  }
  traffic { # <5>
    percent         = 100
    latest_revision = true
  }
}
----
<1> This is the Cloud Run service resource we named `cloud_minio`
<2> We are naming the actual service with a _workspace-friendly_ and non-clashing name within GCP to avoid conflicting with other attendees
<3> This image is the same as using `minio/minio:latest`. However, to run containers in Cloud Run, one must push them to link:https://cloud.google.com/artifact-registry[Google Artifact Registry] first
<4> As you can see, we are referring to a project with new command line arguments to tell `minio` to run in `gateway gcs` mode
<5> This block simply tells Cloud Run to migrate all traffic to the latest revision every time you redeploy

✏ Create a new file called `iam.tf` and add the following resource:

[.copy]
[source,hcl]
----
resource "google_cloud_run_service_iam_member" "member" { # <1>
  location = google_cloud_run_service.cloud_minio.location
  project  = google_cloud_run_service.cloud_minio.project
  service  = google_cloud_run_service.cloud_minio.name # <2>
  role     = "roles/run.invoker"
  member   = "allUsers" # <3>
}
----
<1> This is Cloud Run specific IAM resource allowing you to configure fine-grained access to a cloud run service
<2> As you can see, we are referring to the service we just defined above
<3> The `allUsers` member allows any user on the Internet to reach your container

✏ Add the following line to your `outputs.tf`: 

[.copy]
[source,hcl]
----
output "url" { # <1>
  value = google_cloud_run_service.cloud_minio.status.*.url # <2>
}
----
<1> This output is meant for you to easily see the access URL of your deployed container
<2> The URL is exposed by the `google_cloud_run_service`. However, the `status` attribute is a list that may contain several URLs. Therefore, to output all URLs, the `*` pattern is used

✏ Apply the new configuration and let the magic happen: 

[.copy]
[source,bash]
----
Λ\: $ terraform apply -auto-approve
----

(Output)

[source,bash]
----
random_pet.minio_user: Refreshing state... [id=steadily-proven-octopus]
random_password.minio_password: Refreshing state... [id=none]
google_storage_bucket.bucket_tf: Refreshing state... [id=terraform-course-agent-k-expand]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following
symbols:
  + create

Terraform will perform the following actions:

  # google_cloud_run_service.cloud_minio will be created
  + resource "google_cloud_run_service" "cloud_minio" {
      + autogenerate_revision_name = false
      + id                         = (known after apply)
      + location                   = "europe-west6"
      + name                       = "minio-agent-k-expand"
      + project                    = (known after apply)
      + status                     = (known after apply)

      + metadata {
          + annotations      = (known after apply)
          + generation       = (known after apply)
          + labels           = (known after apply)
          + namespace        = (known after apply)
          + resource_version = (known after apply)
          + self_link        = (known after apply)
          + uid              = (known after apply)
        }

      + template {
          + metadata {
              + annotations      = (known after apply)
              + generation       = (known after apply)
              + labels           = (known after apply)
              + name             = (known after apply)
              + namespace        = (known after apply)
              + resource_version = (known after apply)
              + self_link        = (known after apply)
              + uid              = (known after apply)
            }

          + spec {
              + container_concurrency = (known after apply)
              + serving_state         = (known after apply)
              + timeout_seconds       = (known after apply)

              + containers {
                  + args    = [
                      + "minio",
                      + "gateway",
                      + "gcs",
                      + "terraform-course",
                      + "--console-address",
                      + "0.0.0.0:9001",
                    ]
                  + command = [
                      + "/usr/bin/docker-entrypoint.sh",
                    ]
                  + image   = "registry.hub.docker.com/minio/minio:latest"

                  + env {
                      # At least one attribute in this block is (or was) sensitive,
                      # so its contents will not be displayed.
                    }

                  + ports {
                      + container_port = 9001
                      + name           = (known after apply)
                    }

                  + resources {
                      + limits   = (known after apply)
                      + requests = (known after apply)
                    }
                }
            }
        }

      + traffic {
          + latest_revision = true
          + percent         = 100
        }
    }

  # google_cloud_run_service_iam_member.member will be created
  + resource "google_cloud_run_service_iam_member" "member" {
      + etag     = (known after apply)
      + id       = (known after apply)
      + location = "europe-west6"
      + member   = "allUsers"
      + project  = (known after apply)
      + role     = "roles/run.invoker"
      + service  = "minio-expand"
    }

Plan: 2 to add, 0 to change, 0 to destroy.

Changes to Outputs:
  + url = (known after apply)
google_cloud_run_service.cloud_minio: Creating...
google_cloud_run_service.cloud_minio: Creation complete after 9s [id=locations/europe-west6/namespaces/terraform-course/services/minio-expand]
google_cloud_run_service_iam_member.member: Creating...
google_cloud_run_service_iam_member.member: Creation complete after 5s [id=v1/projects/terraform-course/locations/europe-west6/services/minio-expand/roles/run.invoker/allUsers]

Apply complete! Resources: 2 added, 0 changed, 0 destroyed.

Outputs:

root_password = <sensitive>
root_user = "steadily-proven-octopus"
url = tolist([
  "https://minio-expand-e3mcfodwjq-oa.a.run.app", # <1>
])
----
<1> The URL where your container is exposed. Yours may differ

✏ Open you favorite browser and visit the URL that was returned to you by `terraform`

✏ Login using the generated credentials

[TIP]
====
Use the *`output` command* to *retrieve the credentials*: 

[.copy]
[source,bash]
----
Λ\: $ terraform output root_password
----
====

✏ Go to the menu:Navigation panel[Bucket] menu

image:minio-bucket-menu.png[]

WARNING: You *may get disconnected* during your browsing within the `minio` Web UI. This is because the *session* when you login is *stored within the container*. Cloud Run being a serverless service, you *almost never hit the same container instance* when *returning* to a page you left dangling a few minutes before. 

NOTE: In a *proper setup* for `minio`, you would have a *dedicated server* to handle the *cache*.

✏ You should see multiple buckets amongst which you should find the one you just created as well as the one holding the backend states: 

image:minio-cloud-buckets-list.png[]

✏ After clicking on the btn:[Browse ⇝] button of your bucket as shown above, you should see an empty bucket. 

It is time to *put some tenders inside*! 

✏ Right click on the following image and save it somewhere on you computer:

image:tenders.jpeg[]

✏ In the Web UI, click on the _upload_ icon as follows add find the tenders image you just saved: 

image:minio-upload-button.png[]

👀 You should see it uploaded as follows: 

image:minio-bucket-with-tenders.png[]

[TIP]
====
To *check* it has been successfully *uploaded to GCP*, *ask your trainer* to show you console, or use the *`gsutil` command* as such: 

[.copy]
[source,bash]
----
Λ\: $ gsutil ls  gs://YOUR-BUCKET-NAME/ # <1>
----
<1> Replace with your own bucket

(Example output)

[source,bash]
----
gs://terraform-course-agent-k-expand/tenders.jpeg
----
====
