<div align="center">
<img src="https://gitlab.com/uploads/-/system/project/avatar/45600820/terraform-training.png" width="200"/>
<h1><code>terraform-course</code></h1>
</div>

This repository holds the content of a two hours workshop which aims to teach the basics of terraform.

## Development

* Install `node` following the [official instructions](https://nodejs.org/en/download/) ;
* Install `antora` *globally* following the [official instruction](https://docs.antora.org/antora/2.3/install/install-antora/) ;
* Run `antora` to build the workshop: 
  ```shell
  antora generate playbook.yml --to-dir dist --clean
  ```
* Open [`dist/index.html`](./dist/index.html) in your favorite browser.

## Environment

This repository holds the necessary `terraform` code to deploy dynamic environments for the duration of a workshop under the [`environment`](./environment) folder. To create an environment, simply run manually the `environment:deploy` job on the `main` branch. Wait for it to finish, and look at the artifacts. There should be:

- A `google-application-credentials.json` file containing credentials of a service account to provide to the attendees so they can use the GCS backend and create a Cloud Run app and a Cloud Storage bucket.
- A `bucket-name.txt` containing the name of the bucket the attendees will use as their GCS backend

The `environment:deploy` triggers automatically a `environment:destroy` job after 12 hours, however you can destroy the environment manually if required.